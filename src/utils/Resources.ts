import * as PIXI from 'pixi.js'
import { Utils } from '../game/core/Utils'

export class Resources {
  public static resourcesMap = []

  static async load(resources: any): Promise<void> {
    return new Promise(resolve => {
      if (Object.keys(resources).length === 0) {
        resolve()
      }

      for (const key of Object.keys(resources)) {
        const data = resources[key]

        if (typeof data === 'string') {
          PIXI.Loader.shared.add(key, data)
        }
      }

      PIXI.Loader.shared.on('progress', (e) => {
      })

      PIXI.Loader.shared.on('complete', () => {
        for (const key of Object.keys(resources)) {
          const data = resources[key]

          if (typeof data !== 'string') {
            const image = PIXI.Loader.shared.resources[key.replace('json', 'png')].data

            Utils.loadAtlasFromJson(key, image, data)
          }
        }

        resolve()
      })
      PIXI.Loader.shared.load()
    })
  }

  static get(key: string): any {
    if (Resources.resourcesMap.hasOwnProperty(key)) {
      return Resources.resourcesMap[key]
    }

    if (key in PIXI.utils.TextureCache) {
      return PIXI.utils.TextureCache[key]
    }

    throw new Error('Resource ' + key + ' not found')
  }
}