/// <reference path='./globals.d.ts'/>
import * as PIXI from 'pixi.js'

import { EventEmitter } from 'events'
import { LevelState } from './game/states/LevelState'

import { Resources } from './utils/Resources'
import { LevelEndsState } from './game/states/LevelEndsState'
import { StateKind } from './game/core/State'
import { StateManager } from './game/core/StateManager'

import bg_1_jpg from '../static/images/bg_1.jpg'
import bg_2_jpg from '../static/images/bg_2.jpg'
import bg_3_jpg from '../static/images/bg_3.jpg'
import bg_4_jpg from '../static/images/bg_4.jpg'
import bg_5_jpg from '../static/images/bg_5.jpg'
import atlas_0_png from '../static/images/atlas_0.png'
import atlas_1_png from '../static/images/atlas_1.png'

const atlas_0_json = require('../static/images/atlas_0.json')
const atlas_1_json = require('../static/images/atlas_1.json')

class App extends EventEmitter {
  private width: number
  private height: number

  private currWidth: number
  private currHeight: number

  private app: PIXI.Application
  private renderer: PIXI.Renderer
  private canvas: HTMLCanvasElement

  private container: PIXI.Container
  private containerCenter: PIXI.Container
  private stage: PIXI.Container

  private stateManager: StateManager

  async init(): Promise<void> {
    this.initCanvas()
    this.initPIXI()

    await this.loadFonts()

    await Resources.load({
      'bg_1_jpg': bg_1_jpg,
      'bg_2_jpg': bg_2_jpg,
      'bg_3_jpg': bg_3_jpg,
      'bg_4_jpg': bg_4_jpg,
      'bg_5_jpg': bg_5_jpg,
      'atlas_0_png': atlas_0_png,
      'atlas_0_json': atlas_0_json,
      'atlas_1_png': atlas_1_png,
      'atlas_1_json': atlas_1_json,
    })

    this.resize()
    this.initStates()

    return Promise.resolve()
  }

  async loadFonts(): Promise<void> {
    return new Promise(resolve => {
      new FontFace('Cartwheel', 'url(data:application/font-woff2;charset=utf-8;base64,d09GMgABAAAAACDEAA8AAAAAWyQAACBlAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0ZGVE0cGh4br0AcIAZgAIJKEQgK8RjXXguBRAABNgIkA4MEBCAFiBEHgWcbn0yzERFsHADEk0eT/V8SOBmDa+M3q8KSCuGxxNPqGfVRNyI+XXaBzxH+am6RhW/17uhyqldsg3LYcPAPfgR2T4TzvBGSzA7Pb7P3Gz7w+VRIStiU0ZSYICjqxC6s1W0uy7W72127cFUuyqvMXZRQiW3YIYPskyyilVoNtUkNPd/fsz13PjUfMHiWNGHjeSJZUyCxF2ED7LbI8mAAQYczK8AoRpgJ4y/M583Zm9Sj9HozfCcDH5J4qJzCArAdl6ecQ4GOS8o1vlq2A5MMhGgAWbIWCuAf/l1bScW8LIJl+fIw/vyKhgUP5JcD4aufqgq7w3ZqgbB2gpfuoxrBBH0BZgUQxwAlnWh4LxiABZiewKART3RW6f4zVX84YJlYrlJgwhw4xJgyB5G0q/frj+b09YH09PFYT0dPZj2DDpAy1+VmeP8MCBlgdHWZs19OMocuBwlyiJHLUeLQdZGDJHIWcRz6ZbkoYmu4gy0iG5Hn9dctsmXgm3NTL3KlhFwaSihsr3qnAAAQAMDrj+gMAAB4svt47kA8SmyCeJswCKSZ0VsAqwKApJIIereMDQJGVAaEJP3qGLnPIDILMrsiBHsPSEoAtAsVgywT1h7eXNvNuuExaL1QmA93wzP6KnCkCFmKTMXf2Y2+UmXY5nqs/m8vPoE/dUmimzg7FsbMvOfBqBvYvpk/28ZO+9OOWDaWGi7OlIZQbkbdQe1tOk8D4A1qCLfgEbKHBgMuAormNUurZ1/EV06RD4pLiYOqinZa66joBr2f4Y2mHCwvq1hbX3bB8QS0AAliICE6YQY8HIfbcC+Ppye8Lu+PD/yAJKJyRNspuRBPGkEn4wYrtCfH+HwHb/BL8BSCN1YNqZsUxmGyzYWwbNhbACNQgRBKllEAjTaIXjERFZCgwUGDDgs2HLjw4CO4rLghGxGClObGS1UbwmiXMzb6OF4V+oO/3AoN5TIZBIlIdg8uPPgIEIYVnW6MdkaAI4hUJGgLAHExNBiwYMOFz2UC/UWRZhGGZPGEECNCBQESfTOGBgPWhGkg5hclKTQ/uMR+IhWYpgUbbWvyz9r8w1MQDYIYT88YMv4IqZCetJm7SIbZK42BYYeKjhU9axPky/qZRmwdg64sedoZgqOe4Yq7RgMDFjJUycHOEPzzzCX4Tp8MS787IqW5wRwDAIEAWniSA1HNzeQWAAgEiIaQ9NxbxBJdzLa5gwOFticoPgIeWIwutIjA+QKh4n8pJHMDy/xrpJhcaIx4SKO2gKWzRm7IgW3QEyHDmtisw2PIU3jRSyQ7NIokVPCCAtXSzESW+4QWCnZ6v53SEa2Gh5RbnMQDF0OECRcefARuyTkS7sZFBx07DmIeER4cRFgI3BKB5p81JQoQBBimWk4PQWB+VHUpSFJYQkhndAFWeRMR4wIRMjS48N0STwVkFRVwAlUBV7UFnEDMrckMdTVcHFYxytsARhAxLMYGEdEDR2IMDQZMWONXHEkdGmPKO6Jhg3Da5VkGKYO2iISNFmF2i6aqNnbLDMO0jcSC9xR4w7kNDgGpJTcYhmmMxI34DETI+MGA5dQaRHI1ph7g7gAlOLMHChwERUyA7IrcMdmoG0ohDW/a0MzEksPxMM0koAk/AVykjCk2gvExk3HhhI8ALhLnjCCEZSlgpNcCihRi/4ywERRwXMmS4AC9IhBEGijQ5seUQu3LCNO6gCoRS8mU5Mn4BXLZbmxBRHO7pzRfOhbbLXjh/yVC48Dfj+jrp/XHxztD//4u8u1TNJJd6a9SvmSsyK/0ZSmfMnoyvB8WSW+W1D52JXl8WZaTBo1anidZEbuxJ3QTrcrVO1eq4L1zoT+ZNBXLVJBBQA8qAOxdQgSEYGIVpVw1JlYunFcDDehwUe2xjlOfCjp4PpASboQndTJoZAL5KlvRB/Kd0mC92PEm8TD+yi0jJhh0ZmWbMNYb7Mm7/NrKyfr39Yv6d/WC+8M8S+pxvzD+aKrlrcYSy/DwmMWBJbBDpUrFkS4XxauASJFiEqWHXcr4VZKruvdaNRrowLKgABBTgSAAEBKCgDyAocyoQlJUSJEKKw9gEDQVlgkK5SsnKaned28V2XCvnkmw8/G/2gwAW5Orgf4c/C0Fi468EoDcvuhnZBQ4aZYQ/uFvAHmCFZ7fGP7pOmvKOsixo9wlb8NQrqLMKg4NYUuX0KBz06wTBPPz859+VKqrGcLBjUYIBm1KFqeDAEBQDCc+qlny8c2Sz3T4g51Wp481GE3muPiExKTkFIvVZnekpqVnZGZl5+Tm5Ttdbo+3wFdYVFxSWuYPlAdDFZXhquqaBbWRuvoGUByhjU2gEZoPurAFc8qqEwgWAiD+Od4rzfDq2CMAAwiAJgZfO4iqtZ+qZoWj3E+t9iU0bvZf5EDOpOlxvJZkGeWxcmUmm6TVqrRynpaU07TBacvIjuMmCyluHE2SJC9OqzUls9O4LiyWJJMNlM/mq1YnphpqnDFsHRUnzabKU9Icpe7k9CRNcrxar3KIXJ+9uPQh1WrdfJYbJBIFqJy+jImWAR/kP0M+ivEH5U2ykd5KbY8DEnHQoZjzlXmYunn7Wr6lgOQjL6HOwFWx5VBxbsMRWmcRP5IOuVCqlyuGWViP0u6elQf6WGnFfSrb0/H8Ewc1hUo72od4S4x6mCgF/N6D5V+lz4DfkrD9nmEVrYXkEnKc9JcXy1SW24CxYxCZ+MguGkJuaAFZqTRcpcaYtYpRoVMyipZcjjsHNSis1OFDjNGoZnv4DSPbtK7Nvo7oFHjhVENJwNKQA1BZpp/KhADfd/a2lD8rUgnFsNYpZGEP9SNP3cY525ydMJpQHSXtIkdqkt9H2lGZ4mTUUz8mtCxJUhkptXDgIoA6OE1ZbU9kjNxguvVQaagGYCs6jWOk1WJjr7PkJisVWGtUWG+JVz3MhxKt0zKlVB5RnJbGsyts2OZ3A6jnxN8eXvG7KuY3HJCxlkG57O2rOYxcB4w3XeQdQL6zLqBep5gf7f31fqRk5/u03CatCAOAcEwbbD2UDvb3fBrW8uD/OH+DUBCSZz12w1AMnVQXI6ASl8f/lAIrv+d8iCZiTs0pOVEV8ibpU4mtVfo/77Jlq6KIaZvV4yomWah84FvQ4PDPzUxFiqVyilBZqAFjX9qVIaLIwSxidD1BJekzRc5P8oCmWVWMVQcYcnwq6wzqHpNsNlWHVUJUrvmVxd7VtxY3yUZ8aSu1/Sklfa40CsL4XuxTm3NAGEPi1kIJ8cYMVIBizgB5YeLSvk5y0+S4sjTSgHXbrdzI3oL1q7gG7240XAeV8yGllJBMZgBVRCxEWx7JJ7l9XW+K0SbUHfJrjk8ksoK8FipAPbcvMBwQg/3ZFnZy/ebEhj2eK3MMfFlDtnsfClOQuUL+fQl4pbCqxD6dAPr5I9KZ4NkGrOt3swdS2eBxIUhXLtkD+DI2AQekKvRpX6VXW1rZp+vpYywgI2zPD5kr5phXq12o71aQGZn2cQ2hbj1tg27nBrEYEJikbaAh0crR2tVe8BgpElEeiktZpry//wzZyKfaOm1ChbLIzzvAJcZy4nlJYgYz8oh82TPtVlTV/4dTAnYHDDkLNVrF8GiU90OtFXQ0X2JhA9AymNEc1NYbp2APxXzHK237LbU8Aiflw9XLXf4IELaXY0feiuIGAwOnGKsIP9B57WWF9hLPeQORD95iRoIjifsTmkxTtqMf3m4CUivrs6Uoib6Lhq2mGB29Z6n9jFOfZX82HGmY2+SUGZw9heWpI4Fy13lud++Gm7LM/+I8acO4EQZ6tmBv0982QwvkGtDvI4cCqOvR9bIxUHevLCEPCav7kEdeXOT8o/ImsBGpbJWqviV86hugDyYFthg5SzAXVimvm7YGdu7Y0bQqUa9gvyJq5uaWfDjB20DkBDm2llAMf9LPjweV41p3+syoYXV3uJSuCqso90n1vCr+jNJZTTN1m4KVHD9hBYl8km5qanwlrbiXI1slWStePlz1ShTOYhFAMWYhuFTK23wrD5gytS1JWmyfGZy8onG7fHh6u8sBYMwYhR5lmV/HQ+xFyCOufVge5b0VSUoGMVdxFREhDw4Mk3KHqVzVjzHRF8VODpgEBC5mXH6rdrAQj4CqWBYWscPGRnZ6PYbr96tfHeOXlPuRQjQSOTCAtZyv0V5tRZIOfv5hvrXBBpjs/T4PHHS4w5N0xV71ijNhhqZeWIhnJhf2hG6H9adaRjiJrzHLItqUSfz/OCFeCOXUH3HATeIU1mPpSkVJrU0lvIHsmq/VwciBoOjV+eqtwo04hwAmNwkXOYYk32/EzB/XgkDhpn0tLNH27HAd80RQ5ytAlU232bP+M40kTILGVvlk4XIcA01VFVbzVoaDluFIbGYKiJTISKgW6iE3+SPC4sy33CR0inYjTUbrjg1JjuOrzX2jkiAGp2qomp6erzC0A1OusryraaYnVK+4r4JuAuTbbpZW6HSNrcYePCKP7o7Q8lRV4zkHqCWiHJmACxg304xMVxfnO7/d5RmNvjcUK8dtmsMeYyiAzhQd2qkxLaW7bVipbCdW6fsljbJuzu+OXNQ73Ixv7ranJCQyAFRrJGHrwUjXPoZRmf6guzb6cKSPAzYO4sJwt0KhhJNWuNYoJmovnm62LpK/eS3DPzHCXz7v+qb6eLIimQNJYmMzrQyeeK/awMVyrm+LsQOv+/qyShbKOsXoYjCF9g9+Po+LzQgh9GV8MHeLZLg2TnHp0DoWmJlAjplm8Tg3OZsTmMtRtJ0a8J74rHcTZ+nfVvbmpkOhIFQI7LiF9GdWN4OfK0JNcxC6omWGApOeKf2QPWBHy1fMnavhf9QR+3xo27NE3sqcEGpRFItVdXH172WgxfpeZxE+tflsSskGQSjPGGihsaKUdt9MimRlJjqYw4n26DozOOgVhXb+ck7UtBEp4d5q1JBXfX7YGEhNzplw2Ep06ukSvr+cW23Y9NSm6HY3xfCnbfCNT1c5ehgbIyZBt2VrugD3nfoGKZWalJqcQDPDXdGciIVq6fdvnayJPmOSszBbMSTqVlw3m4WWcwtUVGIL8QBq/Vf6Mk74qn2IZbKBEwxsqMrvyeryisaNPkAembbOK35rrVhnOggmMLml7F2/lcJZ8PEMrHt45LpwunTvbcro5TDwFEO1SFpRTcPntky28XOUdrPZTsjtzIWMnq3Tdwux3HbYyo166mZsaJtf+tTON6WiSimednn1Q6UcGXfXKhW3VYacalWW+R/qq/W3OHZc+edFuqEo09n0b8wn3y5s5z9taklO+oVbcDBmr9CKkvvu5uHhMk17tIz7OFaP1UmYyofEnRrVxc8yGIkYpuxUf2NGLVtfEtqEOfrGAEsXKXaHWziux+iRbO313wRKZHvKvadIIQDod2/IKKlTi6/iJso7catQs+8NBSUt1xAh0TFeCefn06qRVJyRJ9xysaMRY6yiS2LkrqPEec8mkI7hyWXigtj1d9xmkvs+yJc/8hJXM/5nuZnV3GIIaVac4IVH7uFwr89S5NaGjtOf7qPDq/P/nDrMJWraOQGeUVStHO1Bn+EX68DRb59K1oty4AcOn/i6q+tcWo6xYIlLQGHSVxVwFDo/JqQRBb85dHpHQJQt4WG1/+WLHuxutjVHOdKi9nbbs0GV5mlddzQRoxDzH09Q2qnMWEgDlcm4LejZ6ZZlByCMwyIfS2C3PNQVKSuyXGES97jdc4BMhREm5pfyqzoA7wjvzTX75cStq9arfJwnLRanrUNzmNArmhFvMVvLjf0VjsKSU7u2fpZFOWBAQ7hs5YWZiUSYjZVJ9cd4VWt5xQtkcBTmZ/pTIRbml8YeRjOZt665rwg9rn9A4vgFHobEk2aR2cTVIwCGcOmpPc+FNNkY1XHHFbjwtTMvMmNksj6f1xEcaYarafmLioKOnNcxCbg96wP6MHffN1+O5DW/qSH4829SjXHJlqJiW4EuUeEqzKqoysys69psdCUVJhSZ+wbKKniZnv8OkoWvcfD1/2i0Sc4be3ZimlhDVr4kq1Cl3/rKGJ0yq9lyOEXNkrU4oL7mXJqxKLgVv4VGy7pcksPKmJlmVmZtnMOnL6WqPWIWzESFucB77TdMmIsyYTY/nuXLNxZYgxorK3o047I8v7Qvemsr3rwpYzGYjIEQkv1jSpTALXaMrE2+6/DIN+z9lyrbLDBqNzAEksb4F9c3MTE4XVSBs4J0+dsMtlcbeHD0ZYYI/Lz87zhCVqsndAIhJMBbtUL9lZvxxPA6PRHDQyGMZeogXqa1m/6OJHHk3Xw+sFtaMOS00LDyNt6m4qk3vrh3jlIumuYhLKGBoWzB+AI9eGHqr8T8/l0qFhdRiJr/SNUfio8B02YNj8hp5su0ZtNfdUmkaFGEh9Bczs9TCCzUX/1WRYDwDro3o1dz8vsAOPzAAVV9WlPkgM9Ud1+ymOm0vSVg7E8ozV26SR3fqX+aLQbCK2WD7bap2vrUe8d7QsHx9tT9kfqMg8t6Lz72qcOf2+1AP85qLsoVPRT2PgLEAB9l1MT8dVzN+STdTJfTQTquwkrI3kgkUFfxn8jXMVbJogqGfRp8eHd7VRxDLFKg3C1PZLK+y0w3h4wcza3UVorz7OFtd0FSzaTJzL66TIEI3lIpOnERaWstHslwtu4bqa6tjox2n9QuQ71gtMPra6oYHuobaMyJl12iULpal9NYTb5nZgYZ5M8VGekSVkmxVa9KMKW9uJVvSkpd2NoHWvqGAo3gcQ3V3JpCbB5SUrWyrIdaK9jLAtRgqV3Chf0iOaeeKY8DEMSn4jfaYBLZwatAPlFydvAt/7wiTEhfWAMmDnblDTq/b++fOSB0qrXehD9iJBmLwiSjNdGuqBk7LLOvXZIibyRoafsjPcCvoPopw5/FqCuuyrK+L2LuRD/+eUuQ9MWBwfvSPFF/fefadhQD8Z9c/dbQ3C+sS5/0B5sdPlqONEMvnVhRSJVaXYKcCevQh4kVlUth7I5F+curWTXPxRCmXfHp4PqGZTH8+pykhBlhL7+R6tpJjXGl/dTVIgHdk5RTKFoACwohzd56zFg/r3e+zm4x1iMPnongYE1ncUZr5cIlfb2LxoJtoeyi9LrgkrEB0DIQDTS8ft8DPWrZpR8dtqE9Uu3xhEeeURo/MmPg41jt55UZWWCNot0HclkL8eFOWc0+NTw7qbdWEQPCMB/rdB9dNYPGT9IXTMXXozpreFTCYMMyulRgVryjlYCek12NfIbJrceHz57+fz5bhgc/pLgvDQ/OVJZffnrZIyFUtbCUwf284PKFJTOx9x8BcysHx8G/IOh+kTOH0MfP33XQe+z4pSUzsccPvVDwOZdRulCFhs6s/OHHvpZrw4MvcakPh+fR3dPPTQ/jereRyW8EJ2QjMSX+phyvzPyVTrrCEBfvkXQak425Wm0zx2OSfzX1vC31F2ad3BOTpXxzp3m+fJZdYH7iI7FMbvgoWLsFvBSxQGcKFfsaaoo2gTN3b/c0NRaKfAQn/kA2JyFiKVr8yLZlN///+Xp0257u5e0Hqkyw7eGw21eeFVqxaiBmjY9VopD0vo465cY8j84wDUfheHeqTgxu/PmUPpxMiQcWK7jkUdXulCXCBfsC0aYbn7M+sYfqRMK9+c64PDkDEb7wi7yJH7qPbpko6n/+P6kLBJ7YW12T//Wo8AqrqR48+Y81wOZXC727wfCSs53GKmvt6ugurRvXNBcRIMArtFBDLONTnwvUL3zj4e/8zd/lqZ0IIU1mzf4NUeGY7i2yzJAmcT8cEi4Hh6f+to936kPBNZxIQY2g3E7H/9uT5ocxIP/k6juG5j5hXdp2f3mTw0fL4KiFvTbLvXZJzjj47TxxLlA0eULZks0ja5wbyktaU0opnfTVV9IZwM8rsFCDpPGpO3zVC0sSCfN//q5FIXco7JFeqBo0IOU0OHrvnMIAX33o01/buAFr1/DCN7ZY/QI92GEoQmM4A5TxT4PQIA6XOOx3B+Otam9z/3vZGMRuL/Rzx8/yCfNdFx/3p+rmNh3yEbcjMDixNsfky8B2ad6dMpVLGPWCtN2i1SwYjqroX5R4vPsc9yok4Cv8jFVDvP0Jc3zuwv2Vjz6qmAUf71udc1+JONWl1pb0xTXqcu3zGCGS5IxbRn+3hoINXiUjzrfYESXFzbhzTSaBykTbb2/2CB973UnGOMbOF+WDL66KrrCb6nefzJH6Ods4zuqOIjH30+k/KkKcLQ28r1HUy16JMXChwAGFAanDfz60rL8yzBz+R4/GiN6HqokVfSzjnwUi0++dEf4kv5F6HSqh1pJG1ivChLSOIAcY3ouhhrgmxmNyEzYVhrgJ+nsQN9pG9sQiz2k5cFeGNcQFz3/O7a4cHCwbRmwFukK9g0G3j9pDMFv/DLTCkLTTnq+B3ArUQkdZtbymN9YBsvrDWwx3eR8lKDx4kfGAjvuujv3JEZG1XOC30sMsA2kQxfoOhNmQU/b0PbpQXVbAxBU5IEVAsvGxQzyMAc5f2vCcJreq7SAr37m/M1VbJvRZuEMs41cikcHAY4xCs6Cbq8zSbxazcdBDEMN4BYTCKGRyZ+qX9q6ij/nzhiWOJQdMFPnpkfAyENlfy7sA6TEvjSlYT0gQCIVI4593VAzxIePYoUO119J9RzOyXl5+Z50kXTwVJqp2ATOCNZP95a5w3ZuXjifH9UDLb3sMjyRru/0WephlYqSKxG8YHrpwYclrjiJogDiAu5m9K2t6y/HeC8Fwknqfp5qKJOeXjqd563XD+Y3e6e1DjSQd1MWbBEVDeoT6PqdgoMidW5fpBIWdPq+7Bbx3qntx/bAxwKrzNyvsVF5uPyKXagdI588FvxJIEvISGmU14LoKyQGjzAsSl22eyWx+Yhghv53BI1Wlo6meiHYor9F3dOdwE/mAT/FFcdK3cTQMIzAEJ8Usig9+ylbdb/IIplErjj1BX+YpHd78hhwPKO0v9hV0grcWPMJE0uFlVz5lcxnn86fP++9pjg2iXldUIJdp05l0OV0sG11Mb/02XFIoepnGjsxqQmmGwAeL554s2SlnI6dR2ZeCd1T6KYfeBJ77cZFGVvzpa206ey2TMYU5H3wZt2A984+lOc0+r9vuF3+llkRbbq0MhLRGrCGvVD9brYoVyNwb/1zJlS5ob7e9F1Tr3/7JF6qwOhXrTJIQena69SqvzAxYY0E23kykFwoGp/VcJArTJ04yWugqSYKcOYUOIDWsRVRQN/6clg13FborKH8TL0hItrf1gj9+JCRsZ4eE7Ra26/+90/cmDHHelibT/RzN7H6R+ZUfWnO//mbjFXlq/ettSpUUSaGHSMOf56Xq3zZEQMq2OqFgV37AdkzS+5RyIiq9qxmCcp+40LmoM7l29H4OyT7orXV/qxt8xDihWQyNcoIdF5Q/fWIH1l8eGhBzVdrwk5FCYeTFQ3selhvWQrw5SYPkrmqLdoWyDepIce5964kCY9qCwif1Vara/n/FyXl/M3+I9Kge0Q38IV2imDp4XZmmvH63+kn12E/6jQfvUexSe5kkrQD+Ja+x8OIk8OwVAQf9+cMvPs+NtB2JE0AWSC3cwocQiD7/ylzV8jT/m/6av3ncl97WskcrOv9i5NVv9+YbvelteRzGauJXIatg60vg9KULHwrU8WXBjdsaBZcK8uhf1/Fe8cdIHrxreawA7PKFLmqeX6fCz9mgN77IrLy9wdGxg0+Az9ZMxBKAkdI6cfLVQFXjOH+vzKBxfPhlV7dcrZj8bDQygwppvWbqwvFzO8YfLIanzVK/NsZtbHXY0jxt9Ago7I+VGGe4F/Eg/+VkKMArslBRlnE2hDE6cHPmhv/sTLl2HYGFJ5OI9RO3UM5nEBi8FUlv6lcdiBdxnv5j4PKV0Lm0vp4n9pGwCY6sdzJEgYvovsOA3U+q0FsTxPqkyTBXvI746Q4Ck1z7fxsy49pxBhaa0pHsEXsRLwAlv8wP4he5M0Zw/L4rMhzS3cSZl86vl+3iR759rHZm8VO7cf74LMn4k3qi7uz7AH5XKZCdKdc4QYGy0sw4IxZZKJ5Ae9hYlcHmH+gWWtMfEJFacdxbhDpLWaDz9g6WqFhK1uSLCY7+6AzvNrZuewiYwhBYpTJzCQt5+/kLgRWG75+8Zd9qVmv4S0g3ff9s+iNxIdoSzemc+R0A1f+fXlBMbnCglEXfwvUiKBjaXvOu4/qvvHFuzp1hIV99t5arB/OrIg3+nhRggCukPgObrh1l3tpPFm5L7bg/02cAwBfA6YFqFIDVJcjxAPYkAaF9VHUDUd2A3wFYwwekv+ybhWUGq1nI+xWV3pT/OzfHd1SMFnfwBn8c7xIzWG4QUlmBVI3MZzSGGUt1qtSYuua55ix9/o+g0UTvb6Ks2QJzPHFYA3g9f14ScqM6M80n1of3Afy47TH7X7UwIJtFOHgDTwC/G5OwdCtvyKZKiA1b3A27BZiggw6AE2iWZqStMV30WocwXdNhhDd0BC9YRzEz6xisPMKj4gU6Qd1DOgOrd4iJxf/rpPRZOjwbf94iO+u8MRsHgVCH8B3RYZQndITOBzqKn1DHkNkI52KvTsirVWcQN0dMLP5CJw0O+fCwxc5ZPIZELTGiR5duYzTM2sTRsLEcKPPitVrCWtiQgcvy6DBoVIvBAiVvNJd+/YkGePQWdRg6aOGU2lPjgf+eVrotTvPnR15VeGhA4+kYHG0Z7PmfXJrjZaM/ckGUugVajBizSLcOHfqBn/Mnv1uXB0CFC7uM61dwwO+RlB8An3IBYWVc3LIaY7pNnwILBKkCbfBnrsfLXO6s8rCvYMHfsT5GekKrZJZVC7BeLJMxUk1r9CBh9qHRRrRo5wF16vOLO6QTV0OS//cs+Y2sxM+oGqGvJgdUGh+8YMyT90YW8KIVJW+E/om1Zdz2iA2+J1r/saGBtnpK2659AYAVxzi/Bpuylu/qV/Lt/dIYhsuqWNg4KFw0Hv6X2EOUX0JKRk5BKYaKGkMtHf0XK/lGphzxj16iJMlSWFjZ2DmkSpMuQ6Ys2XLkypPPaaPs+XIm36cwRwmVoR9Q/gVI97hCpbAq1WosUCuiTr0GZ50Lhj0VAAAA)')
        .load()
        .then((fontFace) => {
          document.fonts.add(fontFace)
          resolve()
        })
    })
  }

  initStates(): void {
    this.stateManager = new StateManager(this.containerCenter)

    this.stateManager.addState(StateKind.COMPLETE, LevelEndsState)
    this.stateManager.addState(StateKind.LEVEL, LevelState)

    const backgroundMargin = 10
    const menuProps = { backgroundMargin }
    const levelProps = { backgroundMargin, index: 0 }

    //this.gameStateManager.setState(GameStateType.MENU, menuProps)
    //  .on('play', () => this.gameStateManager.setState(GameStateType.LEVEL, levelProps))
    const levelState = this.stateManager.setState(StateKind.LEVEL, levelProps) as LevelState

    levelState.start()
  }

  async start(): Promise<void> {
    this.resize()

    this.app.start()
    this.app.ticker.add(this.update, this)

    window.addEventListener('resize', () => this.resize)
    window.addEventListener('orientationchange', () => this.resize)

    return Promise.resolve()
  }

  initCanvas() {
    this.currWidth = this.width
    this.currHeight = this.height

    this.canvas = document.getElementById('canvas') as HTMLCanvasElement
    this.canvas.width = this.width
    this.canvas.height = this.height
  }

  initPIXI() {
    this.app = new PIXI.Application({
      view: this.canvas,
      resolution: 2,
      antialias: true,
      backgroundColor: 0xffffff,
      resizeTo: window,
    })

    this.renderer = this.app.renderer
    this.stage = this.app.stage

    this.container = new PIXI.Container()
    this.stage.addChild(this.container)

    this.containerCenter = new PIXI.Container()
    this.container.addChild(this.containerCenter)
  }

  resize(): void {
    this.currWidth = this.app.screen.width
    this.currHeight = this.app.screen.height

    this.canvas.width = this.currWidth
    this.canvas.height = this.currHeight

    let currWidth = this.currWidth / this.renderer.resolution
    let currHeight = this.currHeight / this.renderer.resolution

    this.containerCenter.position.set(currWidth / 2, currHeight / 2)

    if (this.stateManager) {
      this.stateManager.resize(currWidth, currHeight)
    }
  }

  update(dt): void {
    if (window.innerWidth !== this.currWidth || window.innerHeight !== this.currHeight) {
      this.resize()
    }

    if (this.stateManager) {
      this.stateManager.update(dt)
    }
  }
}

export {
  App,
}