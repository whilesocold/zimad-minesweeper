import { App } from './App'

const app = new App()

declare global {
  interface Window {
    app: any
  }
}

window.app = window.app || app

document.addEventListener('DOMContentLoaded', async () => {
  await app.init()
  await app.start()
})

window.addEventListener('beforeunload', async (e) => {
}, false)