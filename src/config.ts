export default {
  'levelTime': 60,
  'okText': 'НАТЮРМОРТ',
  'cancelText': 'НЕ НАТЮРМОРТ',
  'introText': 'Натюрморт это, или нет? \n Если натюрморт, игрок жмет на зеленую кнопку; если нет – на красную',
  'successText': 'Ты справилась с заданием гоблина, и ему ничего не остается, как пропустить тебя к следующим испытаниям.\n «Но не радуйся слишком рано, - злобно шипит Гоблин тебя в след, - мы с тобой ещё встретимся, и тогда посмотрим, кто кого!',
  'failedText': 'Увы, ты не справился с заданием гоблина.\nНо внезапно в его холодное, злобное сердце закралась доброта: Гоблин соглашается пропустить тебя.\n «Но смотри, - ворчит он, - не разочаруй меня: со следующими заданиями ты должен совладать гораздо лучше.»',
  'pictures': [
    { 'sprite': 'picture_1.jpg', 'stillLife': false },
    { 'sprite': 'picture_2.jpg', 'stillLife': true },
    { 'sprite': 'picture_3.jpg', 'stillLife': false },
    { 'sprite': 'picture_4.jpg', 'stillLife': false },
    { 'sprite': 'picture_5.jpg', 'stillLife': true },
    { 'sprite': 'picture_6.jpg', 'stillLife': false },
    { 'sprite': 'picture_7.jpg', 'stillLife': false },
    { 'sprite': 'picture_8.jpg', 'stillLife': true },
    { 'sprite': 'picture_9.jpg', 'stillLife': false },
    { 'sprite': 'picture_10.jpg', 'stillLife': true },
  ],
}